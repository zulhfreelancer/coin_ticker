# -----------------------------------------------------------------------
# NOTE: 
# * To make things simple, I'm putting everything in controller here.
# * In real world, we can slice and dice this to models, helpers & etc.
# -----------------------------------------------------------------------

class TickerMinutelyV4Controller < ApplicationController
  def before
    @chart_data = get_original_implementation
  end

  def after
    rates = TickerDailyAverage.select("timestamp as x, price as y").where(coin_id: "bitcoin").where("timestamp > ?", 3.months.ago).order(:timestamp)
    @chart_data = rates.to_json
  end
end
