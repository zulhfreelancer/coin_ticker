# -----------------------------------------------------------------------
# NOTE: 
# * To make things simple, I'm putting everything in controller here.
# * In real world, we can slice and dice this to models, helpers & etc.
# -----------------------------------------------------------------------

class TickerMinutelyV3Controller < ApplicationController
    def before
        @chart_data = get_original_implementation
    end

  def after
    sql = "SELECT TO_CHAR(timestamp,'YYYY-MM-DD') AS x, TRUNC(AVG(price)::numeric,2) AS y FROM ticker_minutely_v2s WHERE coin_id = 'bitcoin' AND timestamp > CURRENT_DATE - INTERVAL '3 months' GROUP BY x"
    result = ActiveRecord::Base.connection.exec_query(sql)
    @chart_data = result.to_a.to_json
  end
end
