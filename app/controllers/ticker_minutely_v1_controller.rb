# -----------------------------------------------------------------------
# NOTE: 
# * To make things simple, I'm putting everything in controller here.
# * In real world, we can slice and dice this to models, helpers & etc.
# -----------------------------------------------------------------------

class TickerMinutelyV1Controller < ApplicationController
  def before
    @chart_data = get_original_implementation
  end

  def after
    data = []
    all = TickerMinutelyV1.where(coin_id: "bitcoin").where("timestamp > ?", 3.months.ago).group("TO_CHAR(timestamp,'YYYY-MM-DD')").average(:price)
    all.each do |item|
        data << {x: item[0], y: item[1].truncate(2)}
    end
    @chart_data = data.to_json
  end
end
