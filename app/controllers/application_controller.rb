# -----------------------------------------------------------------------
# NOTE: 
# * To make things simple, I'm putting everything in controller here.
# * In real world, we can slice and dice this to models, helpers & etc.
# -----------------------------------------------------------------------

class ApplicationController < ActionController::Base
    private
 
    def get_original_implementation
        data = []
        all = TickerMinutelyV1.where(coin_id: "bitcoin").where("timestamp > ?", 3.months.ago)
        grouped_by_day = all.group_by{|x| x.timestamp.strftime("%Y/%m/%d")}
        dates = grouped_by_day.keys
        dates.each do |date|
            daily_average = (grouped_by_day[date].sum{|key| key["price"]} / grouped_by_day[date].size).truncate(2)
            data << {x: date, y: daily_average}
        end
        return data.to_json
    end
end
