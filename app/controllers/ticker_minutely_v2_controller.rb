# -----------------------------------------------------------------------
# NOTE: 
# * To make things simple, I'm putting everything in controller here.
# * In real world, we can slice and dice this to models, helpers & etc.
# -----------------------------------------------------------------------

class TickerMinutelyV2Controller < ApplicationController
    def before
        @chart_data = get_original_implementation
    end

    def after
        @chart_data = Rails.cache.fetch("btc-rates", expires_in: 12.hours) { get_original_implementation }
    end
end
