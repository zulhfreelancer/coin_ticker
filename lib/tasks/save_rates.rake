require 'csv'
desc "Take BTC rates from JSON file and save to database"

task :save_rates => :environment do
    file_abs_paths = Dir.glob("#{Rails.root}/lib/tasks/exchange-rates/**/*").reject { |f| File.directory?(f) }

    file_abs_paths.each do |file_abs_path|
        puts "Saving #{File.basename(file_abs_path)} records ..."
        coin_name = File.basename(file_abs_path, ".*")
        rows = CSV.read(file_abs_path)
        rows.each do |row|
            timestamp = row[0]
            date = row[1]
            price = row[4]

            next if timestamp == "Unix Timestamp"
            next if date == "https://www.CryptoDataDownload.com"
            
            begin
                TickerMinutelyV1.create(
                    coin_id: coin_name,
                    price: price,
                    timestamp: DateTime.strptime(timestamp,"%Q")
                )
            rescue
                # Catch errors
            end
        end
    end
end
