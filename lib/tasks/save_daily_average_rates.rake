desc "Calculate coins daily average price and save to database"

task :save_daily_average_rates => :environment do
    coins = TickerMinutelyV1.distinct.pluck(:coin_id)

    coins.each do |coin|
        puts "Saving daily average rates for #{coin} ..."
        sql = "SELECT TO_CHAR(timestamp,'YYYY-MM-DD') AS formatted_timestamp, TRUNC(AVG(price)::numeric,2) AS formatted_price FROM ticker_minutely_v1s WHERE coin_id = '#{coin}' AND timestamp > CURRENT_DATE - INTERVAL '12 months' GROUP BY formatted_timestamp"
        result = ActiveRecord::Base.connection.exec_query(sql)
        rates = result.to_a
        rates.each do |rate|
            TickerDailyAverage.create(
                coin_id: coin,
                price: rate["formatted_price"],
                timestamp: DateTime.parse(rate["formatted_timestamp"])
            )
        end
    end
end
