Rails.application.routes.draw do
  get 'ticker_minutely_v1/before'
  get 'ticker_minutely_v1/after'
  get 'ticker_minutely_v2/before'
  get 'ticker_minutely_v2/after'
  get 'ticker_minutely_v3/before'
  get 'ticker_minutely_v3/after'
  get 'ticker_minutely_v4/before'
  get 'ticker_minutely_v4/after'
end
