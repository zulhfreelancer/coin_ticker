class AddCompositeIndexToTickerDailyAverage < ActiveRecord::Migration[6.0]
  def change
    add_index :ticker_daily_averages, [:coin_id, :timestamp], unique: true
  end
end
