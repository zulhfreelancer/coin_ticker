class CreateTickerDailyAverages < ActiveRecord::Migration[6.0]
  def change
    create_table :ticker_daily_averages do |t|
      t.string :coin_id
      t.datetime :timestamp
      t.float :price

      t.timestamps
    end
  end
end
