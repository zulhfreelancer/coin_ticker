class AddCompositeIndexToTickerMinutelyV2 < ActiveRecord::Migration[6.0]
  def change
    add_index :ticker_minutely_v2s, [:coin_id, :timestamp], unique: true
  end
end
