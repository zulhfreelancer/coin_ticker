class CreateTickerMinutelyV2s < ActiveRecord::Migration[6.0]
  def change
    create_table :ticker_minutely_v2s do |t|
      t.string :coin_id
      t.datetime :timestamp
      t.timestamps
      t.float :price
    end
  end
end
