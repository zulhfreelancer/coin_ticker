class AddPriceToTicker < ActiveRecord::Migration[6.0]
  def change
    add_column :ticker_minutely_v1s, :price, :float
  end
end
