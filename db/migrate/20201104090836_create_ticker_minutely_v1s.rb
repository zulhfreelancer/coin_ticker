class CreateTickerMinutelyV1s < ActiveRecord::Migration[6.0]
  def change
    create_table :ticker_minutely_v1s do |t|
      t.string :coin_id
      t.datetime :timestamp

      t.timestamps
    end
  end
end
