# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_05_172753) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ticker_daily_averages", force: :cascade do |t|
    t.string "coin_id"
    t.datetime "timestamp"
    t.float "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["coin_id", "timestamp"], name: "index_ticker_daily_averages_on_coin_id_and_timestamp", unique: true
  end

  create_table "ticker_minutely_v1s", force: :cascade do |t|
    t.string "coin_id"
    t.datetime "timestamp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "price"
  end

  create_table "ticker_minutely_v2s", force: :cascade do |t|
    t.string "coin_id"
    t.datetime "timestamp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "price"
    t.index ["coin_id", "timestamp"], name: "index_ticker_minutely_v2s_on_coin_id_and_timestamp", unique: true
  end

end
