## DB Dump File

To speed up bootstrap process, download DB dump file [here](https://drive.google.com/file/d/1GLUlRZZa6jOga4PT6y2_u7jDICrOG2SK/view?usp=sharing) and restore it using the following command:

```
$ rake db:create
$ psql coin_ticker_development < coin_ticker_development.sql
```

## Performance Fixes Proposal

1. ActiveRecord group and average
2. Enable caching
3. Plain SQL and composite index
4. Save daily average rates in separate table
